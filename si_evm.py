#!/usr/bin/env python3
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
"""Python USB driver for Silabs Clock Generator EVMs"""
import argparse

import usb.core
import usb.util

class SiEvm:
    """Silabs EVM device abstraction"""
    EP_OUT = 0x02
    EP_IN = 0x82

    def __init__(self):
        self._dev = usb.core.find(idVendor=0x10c4, idProduct=0x8999)
        if self._dev is None:
            raise ValueError('Device not found')
        self._dev.set_configuration(1)

        # required initializiation control transfers
        self._dev.ctrl_transfer(0x40, 0x02, 0x0004, 0x00, 0x00)
        self._dev.ctrl_transfer(0x40, 0x02, 0x0002, 0x00, 0x00)
        self._dev.ctrl_transfer(0x40, 0x02, 0x0001, 0x00, 0x00)

    def _usb_cmd(self, data, resp_len):
        self._dev.write(self.EP_OUT, data)
        resp = self._dev.read(self.EP_IN, resp_len + 1)
        if resp[0] != 1:
            raise ValueError("Unexpected return code.")
        return resp[1:]

    def _spi_set_address(self, addr):
        self._usb_cmd([0x20, addr], 0)

    def _spi_write_byte(self, data):
        self._usb_cmd([0x21, data], 0)

    def _spi_write_byte_and_increment(self, data):
        self._usb_cmd([0x22, data], 0)

    def _spi_read_byte_and_increment(self):
        data = self._usb_cmd([0x24], 1)
        return data[0]

    def read_reg(self, addr):
        """Reads a single register of the clock generator"""
        self._spi_set_address(0x01)
        self._spi_write_byte(addr >> 8)
        self._spi_set_address(addr & 0xff)
        res = self._spi_read_byte_and_increment()
        return res

    def write_reg(self, addr, val):
        """Writes a single register of the clock generator"""
        self._spi_set_address(0x01)
        self._spi_write_byte(addr >> 8)
        self._spi_set_address(addr & 0xff)
        self._spi_write_byte_and_increment(val)

    def write_regfile(self, filename):
        """Writes a register map file exported from Silabs ClockBuilder"""
        with open(filename, 'r') as regfile:
            for line in regfile:
                if not line.startswith("0x"):
                    continue
                addr, val = [int(x, 16) for x in line.split(",")]
                self.write_reg(addr, val)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("regfile", help="Register file")
    args = parser.parse_args()

    print("Connecting to EVM ...")
    evm = SiEvm()
    print("Writing register file %s ..." % args.regfile)
    evm.write_regfile(args.regfile)
    print("Done.")
