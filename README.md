# Silabs Clock Generator / Jitter Cleaner USB interface driver

As the Silabs ClockBuilder software is available on Windows only, no simple way of programming their EVMs in a Linux environment is available.
This software exists to at least partially close the gap. It can be used to program register files exported by ClockBuilder to the EVM via the standard USB interface.

## Dependencies
The software requires Python3, PyUSB and libusb 1.0. Different USB backends than libusb 1.0 might work, are however untested.

## How to use
### Data Export from ClockBuilder
  * Create and configure your EVM project.
  * Select "Export" and select the "Register File" tab.
  * Choose "CSV File", enable both tick boxes ("Include Summary Header" and "Include pre- and post-write control register writes").
  * Finally, click "Save to File..." and transfer the resulting file to the target Linux machine.

### EVM Programming - Standalone Mode
To use this software as a standalone programming tool, simply use the following command:

```bash
$ ./si_evm.py name_of_file.txt
```

### EVM Programming - Software Integration
The programming tool can be easily integrated into other python software. Use the following code snippet to accomplish that:

```python
import si_evm

evm = si_evm.SiEvm()  # finds and connects to the EVM
evm.write_regfile("name_of_file.txt")  # program the specified register file
```

Or use some of the lower level methods to read and write individual registers of the clock generator.


